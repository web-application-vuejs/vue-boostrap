module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs60160149/learn_bootstrap/'
    : '/'
}
